<?php

namespace Drupal\findit_migration_utils\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Vanderlee\Sentence\Multibyte;
use Vanderlee\Sentence\Sentence;

/**
 *
 * @MigrateProcessPlugin(
 *   id = "findit_body"
 * )
 */
class FindItBody extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $value = Multibyte::cleanUnicode($value);
    $type = $this->configuration['type'];

    // If the string is already less than 200 characters no need to do anything.
    if (strlen($value) < 200) {
      $summary = $value;
      $body = '';
    }
    else {
      // The summary must be 200 characters top.
      $summary = substr($value, 0, 200);
      $sentence	= new Sentence;
      $sentences = $sentence->split($summary);

      // If it wasn't possible to get the first sentence then leave empty
      // the summary. (If the fist sentence is the whole summary, that
      // means that wasn't possible to extract a sentence from the original
      // text).
      if ($summary == $sentences[0]) {
        $summary = '';
        $body = $value;
      }
      else {
        $summary = $sentences[0];
        $body = trim(str_replace($summary, '', $value));
      }
    }

    if ($type == "summary") {
      return $summary;
    }
    else {
      return $body;
    }
  }

}
